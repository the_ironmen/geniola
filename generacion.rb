require 'uri'
require 'net/http'
require 'time'

url = URI("https://demanda.ree.es/WSVisionaCanarias/wsCanariasService?WSDL=")

http = Net::HTTP.new(url.host, url.port)
http.use_ssl = true

request = Net::HTTP::Post.new(url)
request["content-type"] = 'text/xml'
fecha = "2017-03-29"

midnight = Time.parse('2017-03-30 20:02:40 +0100')
secondsFromMidnight = Time.now - midnight
secondsFromMidnight
clave = secondsFromMidnight.to_i
puts clave

curva = "GCANARIA"

request.body = "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" 
xmlns:xsd=\"http://www.w3\n.org/2001/XMLSchema\" 
xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n  
<SOAP-ENV:Body>\n    
  <tns:demandaGeneracion30 xmlns:tns=\"http://ws.wsDemanda24.ree.es/\">\n      
    <fecha>#{fecha}</fecha>\n      
    <clave>#{clave}</clave>\n      
    <curva>#{curva}</curva>\n    
  </tns:demandaGeneracion30>\n    
</SOAP-ENV:Body>\n</SOAP-ENV:Envelope>"

response = http.request(request)
puts response.read_body

